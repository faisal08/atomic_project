<?php
namespace App\Model;
use PDO;
use PDOException;
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/5/2016
 * Time: 2:02 PM
 */
class Database
{

    public $DBH;
    public $dsn="mysql:dbName=atomic_project_b36;dbHost=localhost";
    public $user="root";
    public $password="";
    public function __construct()
    {
        try{

            $this->DBH= new PDO($this->dsn,$this->user,$this->password);
            echo "connected";
        }
       catch(PDOException $e){
           echo "connection fail...".$e->getMessage();
       }
    }

}